import pytest
import xmltodict
from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_CREATING, SERVER_STATE_UP
from core.models import ExternalRoom, Meeting, SchedulingStrategy, Server
from core.views.api import bbb_xml_meeting_running_false, bbb_xml_response


@pytest.fixture(scope="function")
def scheduling_strategy():
    return SchedulingStrategy.objects.create(name="Example", token_registration="REGISTRATION_TOKEN")


@pytest.fixture(scope="function")
def bbb_server(scheduling_strategy):
    return Server.objects.create(
        scheduling_strategy=scheduling_strategy,
        dns="bbb-example.example.org",
        state=SERVER_STATE_UP,
        shared_secret="EXTRA_SECRET_SECRET",
    )


@pytest.fixture(scope="function")
def bbb_server2(scheduling_strategy):
    return Server.objects.create(
        scheduling_strategy=scheduling_strategy,
        dns="bbb-example-2.example.org",
        state=SERVER_STATE_UP,
        shared_secret="EXTRA_SECRET_SECRET_2",
    )


@pytest.mark.django_db
def test_bbb_initialization_request(client, scheduling_strategy):
    response = client.get(f"/core/{scheduling_strategy.name}/bigbluebutton/api/")
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["version"] == "2.0"

    response = client.get("/core/NotAvailable/bigbluebutton/api/")
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "unknownResource"
    assert content["response"]["message"] == "Your requested resource is not available."


@pytest.mark.django_db
def test_bbb_api_create(testserver_tenant, client, scheduling_strategy, mocker):
    response = client.get(f"/core/{scheduling_strategy.name}/bigbluebutton/api/create?checksum=123123123")

    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"
    ExternalRoom.objects.create(
        meeting_id="random-4425951",
        name="random-4425951",
        state=ROOM_STATE_CREATING,
        scheduling_strategy=scheduling_strategy,
    )
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/create?allowStartStopRecording=true&attendeePW=ap&"
        f"autoStartRecording=false&meetingID=random-4425951&moderatorPW=mp&name=random-4425951&record=false&"
        f"voiceBridge=72790&welcome=%3Cbr%3EWelcome+to+%3Cb%3E%25%25CONFNAME%25%25%3C%2Fb%3E%21&"
        f"checksum=63f74d17fafc7695465d20edcbd64a382684f2b6"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "duplicateWarning"
    assert (
        content["response"]["message"] == "This conference was already in existence and may currently be in progress."
    )
    ExternalRoom.objects.get(meeting_id="random-4425951").delete()

    def mock_meeting_create(request, params, room, body):
        class Response:
            text = """
        <response><returncode>SUCCESS</returncode><meetingID>random-4425951</meetingID>
        <internalMeetingID>2ae2adcbbb4472fa567f9284bc7411112213eec5-1627476400234</internalMeetingID>
        <parentMeetingID>bbb-none</parentMeetingID><attendeePW>ap</attendeePW><moderatorPW>mp</moderatorPW>
        <createTime>1627476400234</createTime><voiceBridge>751208885</voiceBridge><dialNumber>18632080022</dialNumber>
        <createDate>Wed Jul 28 12:46:40 UTC 2021</createDate>
        <hasUserJoined>false</hasUserJoined><duration>540</duration>
        <hasBeenForciblyEnded>false</hasBeenForciblyEnded></response>
        """

        return Response()

    mocker.patch("core.views.api.meeting_create", mock_meeting_create)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/create?allowStartStopRecording=true&attendeePW=ap&"
        f"autoStartRecording=false&meetingID=random-4425951&moderatorPW=mp&name=random-4425951&record=false&"
        f"voiceBridge=72790&welcome=%3Cbr%3EWelcome+to+%3Cb%3E%25%25CONFNAME%25%25%3C%2Fb%3E%21&"
        f"checksum=63f74d17fafc7695465d20edcbd64a382684f2b6"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["meetingID"] == ExternalRoom.objects.get(meeting_id="random-4425951").meeting_id


@pytest.mark.django_db
def test_bbb_api_join(client, scheduling_strategy, bbb_server):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/join?fullName=User+8025106&"
        f"meetingID=random-8094211&password=mp&redirect=true&"
        f"checksum=68d377896b4ec2d05aca5e5dd6a851b672b16ca4"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    ExternalRoom.objects.create(
        meeting_id="random-8094211", name="random-8094211", scheduling_strategy=scheduling_strategy
    )
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/join?fullName=User+8025106&"
        f"meetingID=random-8094211&password=mp&redirect=true&"
        f"checksum=68d377896b4ec2d05aca5e5dd6a851b672b16ca4"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    ExternalRoom.objects.filter(meeting_id="random-8094211").update(state=ROOM_STATE_ACTIVE, server=bbb_server)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/join?fullName=User+8025106&"
        f"meetingID=random-8094211&password=mp&redirect=true&"
        f"checksum=68d377896b4ec2d05aca5e5dd6a851b672b16ca4"
    )
    assert (
        response.url == "https://bbb-example.example.org/bigbluebutton/api/join?"
        "fullName=User+8025106&meetingID=random-8094211&password=mp&"
        "redirect=true&checksum=f23edc0989d4678c8859c284442d0a5701f78620"
    )


@pytest.mark.django_db
def test_bbb_api_end(client, scheduling_strategy, bbb_server, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/end?meetingID=random-8094211"
        f"&password=mp&checksum=1c7cc05e1779b80b3e802dcb8da5145fefd7dde4"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    ExternalRoom.objects.create(
        meeting_id="random-8094211",
        name="random-8094211",
        scheduling_strategy=scheduling_strategy,
        state=ROOM_STATE_ACTIVE,
        server=bbb_server,
    )

    def mock_end(self, meeting_id, pw):
        class Response:
            text = """
            <response><returncode>SUCCESS</returncode>
            <messageKey>sentEndMeetingRequest</messageKey>
            <message>A request to end the meeting was sent. Please wait a few seconds,
            and then use the getMeetingInfo or isMeetingRunning API calls to verify that it was ended.</message>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.end", mock_end)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/end?meetingID=random-8094211"
        f"&password=mp&checksum=1c7cc05e1779b80b3e802dcb8da5145fefd7dde4"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "sentEndMeetingRequest"
    assert (
        content["response"]["message"]
        == """A request to end the meeting was sent. Please wait a few seconds,
            and then use the getMeetingInfo or isMeetingRunning API calls to verify that it was ended."""
    )

    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/end?meetingID=random-8094211"
        f"&password=mp&checksum=123123"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_is_meeting_running(client, scheduling_strategy, bbb_server, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/isMeetingRunning?"
        f"meetingID=random-8094211&checksum=8ef990483a9fa34df55e110882447c13bd8072c8"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "false"
    ExternalRoom.objects.create(
        meeting_id="random-8094211",
        name="random-8094211",
        scheduling_strategy=scheduling_strategy,
        state=ROOM_STATE_ACTIVE,
        server=bbb_server,
    )

    def mock_is_meeting_running(self, meeting_id):
        class Response:
            text = """
            <response>
            <returncode>SUCCESS</returncode>
            <running>true</running>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.is_meeting_running", mock_is_meeting_running)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/isMeetingRunning?meetingID=random-8094211"
        f"&checksum=8ef990483a9fa34df55e110882447c13bd8072c8"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["running"] == "true"
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/isMeetingRunning?meetingID=random-8094211&checksum=1234124"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_get_meeting_info(client, scheduling_strategy, bbb_server, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getMeetingInfo?"
        f"meetingID=random-8094211&password=mp&checksum=68af73f93f48eb824712ae2ca1d19481d1b65472"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find a meeting with that meeting ID"
    ExternalRoom.objects.create(
        meeting_id="random-8094211",
        name="random-8094211",
        scheduling_strategy=scheduling_strategy,
        state=ROOM_STATE_ACTIVE,
        server=bbb_server,
    )

    def mock_get_meeting_info(self, meeting_id):
        class Response:
            text = """
            <response>
                <returncode>SUCCESS</returncode>
                <meetingName>random-8094211</meetingName>
                <meetingID>random-8094211</meetingID>
                <internalMeetingID>2784ee866a934d9f27781875918772621e550776-1628182217015</internalMeetingID>
                <createTime>1628182217015</createTime>
                <createDate>Thu Aug 05 16:50:17 UTC 2021</createDate>
                <voiceBridge>674733854</voiceBridge>
                <dialNumber>18632080022</dialNumber>
                <attendeePW>ap</attendeePW>
                <moderatorPW>mp</moderatorPW>
                <running>false</running>
                <duration>540</duration>
                <hasUserJoined>false</hasUserJoined>
                <recording>false</recording>
                <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
                <startTime>1628182217035</startTime>
                <endTime>0</endTime>
                <participantCount>0</participantCount>
                <listenerCount>0</listenerCount>
                <voiceParticipantCount>0</voiceParticipantCount>
                <videoCount>0</videoCount>
                <maxUsers>0</maxUsers>
                <moderatorCount>0</moderatorCount>
                <attendees/>
                <metadata>
                <bn-userid>test-install</bn-userid>
                <bn-meetingid>random-8094211</bn-meetingid>
                <bn-priority>20</bn-priority>
                </metadata>
                <isBreakout>false</isBreakout>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.get_meeting_infos", mock_get_meeting_info)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getMeetingInfo?"
        f"meetingID=random-8094211&password=mp&checksum=68af73f93f48eb824712ae2ca1d19481d1b65472"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["meetingName"] == "random-8094211"
    assert content["response"]["meetingID"] == "random-8094211"
    assert content["response"]["internalMeetingID"] == "2784ee866a934d9f27781875918772621e550776-1628182217015"
    assert content["response"]["createTime"] == "1628182217015"
    assert content["response"]["isBreakout"] == "false"
    assert content["response"]["attendeePW"] == "ap"
    assert content["response"]["moderatorPW"] == "mp"

    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getMeetingInfo?"
        f"meetingID=random-8094211&password=mp&checksum=123123"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_get_recordings(client, scheduling_strategy, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getRecordings?"
        f"meetingID=random-8094211&recordID=random-8094211&checksum=49dae9da43c12c58a18f1dcaa922ee01b58a1000"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "noRecordings"
    assert content["response"]["message"] == "There are no recordings for the meeting(s)."

    def mock_parse_recordings_from_multiple_sources(servers, params):
        xml_response = """
        <response>
        <returncode>SUCCESS</returncode>
        <recordings>
        <recording>
        <recordID>ee95ce67a5ebf69058afdf1b3364e868b6fe59b3-1627994200254</recordID>
        <meetingID>20d80cb64e834e209b5b0909b8c6558a</meetingID>
        <internalMeetingID>ee95ce67a5ebf69058afdf1b3364e868b6fe59b3-1627994200254</internalMeetingID>
        <name>1337</name>
        <isBreakout>false</isBreakout>
        <published>true</published>
        <state>published</state>
        <startTime>1627994200254</startTime>
        <endTime>1627994244463</endTime>
        <participants>1</participants>
        <rawSize>2231777</rawSize>
        <metadata>
        <onlypromptguestsforaccesscode>False</onlypromptguestsforaccesscode>
        <muteonstart>False</muteonstart>
        <accesscode>None</accesscode>
        <disablemic>False</disablemic>
        <roomsmeetingid>86690</roomsmeetingid>
        <logouturl>None</logouturl>
        <streamingurl>None</streamingurl>
        <disableprivatechat>False</disableprivatechat>
        <meetingName>1337</meetingName>
        <creator>fbi1059@rooms.h-da.de</creator>
        <disablecam>False</disablecam>
        <meetingId>20d80cb64e834e209b5b0909b8c6558a</meetingId>
        <moderationmode>ROOM_STARTER</moderationmode>
        <disablenote>False</disablenote>
        <maxparticipants>None</maxparticipants>
        <allowrecording>True</allowrecording>
        <isBreakout>false</isBreakout>
        <guestpolicy>ALWAYS_ACCEPT</guestpolicy>
        <disablepublicchat>False</disablepublicchat>
        <allowguestentry>True</allowguestentry>
        </metadata>
        <size>1554462</size>
        <data/>
        </recording>
        </recordings>
        </response>
        """
        return xmltodict.parse(xml_response)

    mocker.patch("core.views.api.parse_recordings_from_multiple_sources", mock_parse_recordings_from_multiple_sources)
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getRecordings?"
        f"checksum=c43ca7f0753c88f69e9d14b6ffd633ab2b6f1481"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert (
        content["response"]["recordings"]["recording"]["recordID"]
        == "ee95ce67a5ebf69058afdf1b3364e868b6fe59b3-1627994200254"
    )
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/getRecordings?"
        f"meetingID=random-8094211&recordID=random-8094211&checksum=123123"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_publish_recordings(client, scheduling_strategy, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/publishRecordings?"
        f"publish=false&recordID=random-8094211&checksum=cad398f769288fede4eaa924f273da24eebb1d94"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"

    def mock_publish_recordings(servers, params):
        class Response:
            text = """
            <response>
            <returncode>SUCCESS</returncode>
            <published>false</published>
            </response>
            """

        return Response()

    mocker.patch("core.views.api.publish_recordings", mock_publish_recordings)
    Meeting.objects.create(room_name="Test", replay_id="random-8094211")
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/publishRecordings?"
        f"publish=false&recordID=random-8094211&checksum=cad398f769288fede4eaa924f273da24eebb1d94"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["published"] == "false"
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/publishRecordings?"
        f"publish=false&recordID=random-8094211&checksum=123123"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_update_recordings(client, scheduling_strategy, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/updateRecordings?"
        f"recordID=random-8094211&checksum=afa4018d4c9d683242aeb5a7bdb15bafe79c813d"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"

    def mock_update_recordings(servers, params):
        class Response:
            text = """
            <response>
            <returncode>SUCCESS</returncode>
            <updated>true</updated>
            </response>
            """

        return Response()

    mocker.patch("core.views.api.update_recordings", mock_update_recordings)
    Meeting.objects.create(room_name="Test", replay_id="random-8094211")
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/updateRecordings?"
        f"recordID=random-8094211&checksum=afa4018d4c9d683242aeb5a7bdb15bafe79c813d"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["updated"] == "true"
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/updateRecordings?"
        f"recordID=random-8094211&checksum=1231233"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_delete_recordings(client, scheduling_strategy, mocker):
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/deleteRecordings?"
        f"recordID=random-8094211&checksum=ab43b733ad6456f908b6955f2506f10f4c6be014"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"

    def mock_delete_recordings(servers, params):
        class Response:
            text = """
            <response>
            <returncode>SUCCESS</returncode>
            <deleted>true</deleted>
            </response>
            """

        return Response()

    mocker.patch("core.views.api.delete_recordings", mock_delete_recordings)
    Meeting.objects.create(room_name="Test", replay_id="random-8094211")
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/deleteRecordings?"
        f"recordID=random-8094211&checksum=ab43b733ad6456f908b6955f2506f10f4c6be014"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["deleted"] == "true"
    response = client.get(
        f"/core/{scheduling_strategy.name}/bigbluebutton/api/deleteRecordings?"
        f"recordID=random-8094211&checksum=45254654"
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


def test_bbb_xml_response():
    response = bbb_xml_response(
        "SUCCESS", "noRecordings", "There are no recordings for the meeting(s).", recordings=None
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "noRecordings"
    assert content["response"]["message"] == "There are no recordings for the meeting(s)."
    assert content["response"]["recordings"] is None


def test_bbb_xml_meeting_running_false():
    running = bbb_xml_meeting_running_false(True)
    assert running.status_code == 200
    content = xmltodict.parse(running.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "true"

    not_running = bbb_xml_meeting_running_false()
    assert not_running.status_code == 200
    content = xmltodict.parse(not_running.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "false"
