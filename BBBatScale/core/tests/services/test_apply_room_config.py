import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import Room, SchedulingStrategy
from core.services import apply_room_config


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def room_d14_0303(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        meeting_id="12345678",
        name="D14/03.03",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
    )


def test_apply_room_config(room_d14_0303):
    apply_room_config(room_d14_0303.meeting_id, room_d14_0303.config)
    guest_policy = Room.objects.get(meeting_id=room_d14_0303.meeting_id).guest_policy
    assert guest_policy == "ALWAYS_ACCEPT"
