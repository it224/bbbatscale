from urllib.parse import quote

from core.models import GeneralParameter
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpRequest


def general_parameter(request: HttpRequest):
    _tenant = get_current_site(request)
    return {"general_parameter": GeneralParameter.load(_tenant)}


def auth_urls(request: HttpRequest):
    return {
        "login_url": str(settings.LOGIN_URL) + "?next=" + quote(request.get_full_path_info()),
        "logout_url": str(settings.LOGOUT_URL),
    }
