from __future__ import annotations

import logging
import random
import uuid
from collections import defaultdict
from datetime import timedelta
from hashlib import sha1
from math import ceil
from typing import Optional

from core.constants import (
    GUEST_POLICY,
    GUEST_POLICY_ALLOW,
    MODERATION_MODE,
    MODERATION_MODE_MODERATORS,
    ROOM_STATE,
    ROOM_STATE_ACTIVE,
    ROOM_STATE_INACTIVE,
    SCHEDULING_STRATEGIES,
    SCHEDULING_STRATEGY_LEAST_PARTICIPANTS,
    SCHEDULING_STRATEGY_LEAST_UTILIZATION,
    SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED,
    SERVER_STATE_DISABLED,
    SERVER_STATE_ERROR,
    SERVER_STATE_UP,
    SERVER_STATE_WAITING_RESPONSE,
    SERVER_STATES,
    WEBHOOKS,
)
from core.utils import BigBlueButton
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as DjangoUserManager
from django.contrib.sites.models import Site
from django.core import validators
from django.core.validators import MinValueValidator
from django.db import IntegrityError, models, transaction
from django.db.models import Sum
from django.db.models.signals import m2m_changed
from django.db.transaction import atomic
from django.templatetags.static import static
from django.utils.crypto import get_random_string
from django.utils.deconstruct import deconstructible
from django.utils.timezone import now
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


def get_default_room_config() -> RoomConfiguration:
    return RoomConfiguration.objects.get_or_create(name="Default")[0]


def get_default_room_config_id() -> int:
    return get_default_room_config().id


def _random_token() -> str:
    return get_random_string(length=32)


def _random_meeting_id() -> str:
    return uuid.uuid4().hex


@deconstructible
class UnicodeUsernameValidator(validators.RegexValidator):
    regex = r"^[\w.@+-=]+\Z"
    message = _("Enter a valid username. This value may contain only letters, " "numbers, and @/./+/-/_/= characters.")
    flags = 0


class UserManager(DjangoUserManager):
    def _create_user(self, username, email, password, tenant=None, **extra_fields):
        if isinstance(tenant, str):
            tenant = Site.objects.get_by_natural_key(tenant)
        return super()._create_user(username, email, password, tenant=tenant, **extra_fields)


class User(AbstractUser):
    REQUIRED_FIELDS = ["tenant", *AbstractUser.REQUIRED_FIELDS]

    username = models.CharField(
        _("username"),
        max_length=511,
        unique=True,
        help_text=_("Required. 511 characters or fewer. Letters, digits and @/./+/-/_/= only."),
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    theme = models.ForeignKey("Theme", on_delete=models.SET_NULL, null=True)
    personal_rooms_max_number = models.IntegerField(null=True, blank=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    tenant = models.ForeignKey(
        Site,
        on_delete=models.CASCADE,
        related_name="tenant_users",
        help_text=_("Association of users main tenant."),
    )
    # Application parameters
    bbb_auto_join_audio = models.BooleanField(
        default=True,
        help_text=_("If enabled, the process of joining the audio will automatically be started."),
    )
    bbb_listen_only_mode = models.BooleanField(
        default=True,
        help_text=_(
            "If enabled, joining the audio part of the meeting without a microphone will be possible, otherwise"
            " listen-only mode will be disabled. Disabling this will make it possible, in conjunction with skipping the"
            ' "echo test" prompt, to directly join a meeting without selecting further options.'
        ),
    )
    bbb_skip_check_audio = models.BooleanField(
        default=False, help_text=_('If enabled, the "echo test" prompt will not be shown when sharing audio.')
    )
    bbb_skip_check_audio_on_first_join = models.BooleanField(
        default=False,
        help_text=_(
            'If enabled, the "echo test" prompt will not be shown when sharing audio for the first time in the meeting.'
            ' If audio sharing will be stopped, next time trying to share the audio the "echo test" prompt will be'
            " displayed, allowing for configuration changes to be made prior to sharing audio again."
        ),
    )
    # Kurento parameters
    bbb_auto_share_webcam = models.BooleanField(
        default=False,
        help_text=_("If enabled, the process of sharing the webcam (if any) will automatically be started."),
    )
    bbb_record_video = models.BooleanField(default=True, help_text=_("If enabled, the video stream will be recorded."))
    bbb_skip_video_preview = models.BooleanField(
        default=False, help_text=_("If enabled, a preview of the webcam before sharing it will not be shown.")
    )
    bbb_skip_video_preview_on_first_join = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, a preview of the webcam before sharing it will not be shown when sharing for the first time in"
            " the meeting. If video sharing will be stopped, next time trying to share the webcam the video preview"
            " will be displayed, allowing for configuration changes to be made prior to sharing."
        ),
    )
    bbb_mirror_own_webcam = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, a mirrored version of the webcam will be shown."
            " Does not affect the incoming video streams for other users."
        ),
    )
    # Presentation parameters
    bbb_force_restore_presentation_on_new_events = models.BooleanField(
        default=False,
        help_text=_(
            "If enabled, the presentation area will be forcefully restored if it is minimized (viewers only) on the"
            " following actions: changing presentation/slide/zoom, publishing polls, or adding annotations."
        ),
    )
    # Layout parameters
    bbb_auto_swap_layout = models.BooleanField(
        default=False,
        help_text=_("If enabled, the presentation area will be minimized when joining a meeting."),
    )
    bbb_show_participants_on_login = models.BooleanField(
        default=True,
        help_text=_("If enabled, the participants panel will be displayed when joining a meeting."),
    )
    bbb_show_public_chat_on_login = models.BooleanField(
        default=True,
        help_text=_(
            "If enabled, the chat panel will be visible when joining a meeting."
            " The participants panel must also be shown to work."
        ),
    )

    objects = UserManager()

    def __str__(self) -> str:
        if self.display_name:
            return self.display_name
        elif self.first_name and self.last_name:
            return f"{self.last_name}, {self.first_name}"
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        elif self.email:
            return self.email
        return gettext("Unknown")

    def get_max_number_of_personal_rooms(self):
        if self.personal_rooms_max_number is not None:
            return self.personal_rooms_max_number
        else:
            general_parameters: GeneralParameter = GeneralParameter.load(self.tenant)
            if self.groups.filter(name=settings.MODERATORS_GROUP).exists():
                return general_parameters.personal_rooms_teacher_max_number
            else:
                return general_parameters.personal_rooms_non_teacher_max_number

    def get_theme(self) -> Theme:
        theme = self.theme
        if theme is None:
            theme = GeneralParameter.load(self.tenant).default_theme
        return theme

    @property
    def homeroom(self) -> Optional[HomeRoom]:
        with atomic():
            general_parameter = GeneralParameter.load(self.tenant)

            if general_parameter.home_room_enabled and (
                not general_parameter.home_room_teachers_only
                or self.is_superuser
                or self.groups.filter(name=settings.MODERATORS_GROUP).exists()
            ):
                try:
                    return self.homeroom_ptr
                except User.homeroom_ptr.RelatedObjectDoesNotExist:
                    if (
                        general_parameter.home_room_scheduling_strategy is None
                        or general_parameter.home_room_room_configuration is None
                    ):
                        return None

                    logger.debug("Create homeroom for user %s", self.username)
                    try:
                        if self.email and not HomeRoom.objects.filter(name=f"home-{self.email}").exists():
                            room_name = f"home-{self.email}"
                        else:
                            room_name = f"home-{self.last_name}-{sha1(bytes(self.username, 'UTF-8')).hexdigest()[:8]}"

                        homeroom = HomeRoom.objects.create(
                            name=room_name,
                            owner=self,
                            scheduling_strategy=general_parameter.home_room_scheduling_strategy,
                            is_public=False,
                            config=general_parameter.home_room_room_configuration,
                        )
                        homeroom.tenants.add(self.tenant)
                        logger.debug(
                            "Home room created with the following parameters: "
                            + "name=%s owner=%s scheduling_strategy=%s is_public=%s config=%s",
                            room_name,
                            self.username,
                            general_parameter.home_room_scheduling_strategy,
                            False,
                            general_parameter.home_room_room_configuration,
                        )
                        return homeroom
                    except IntegrityError as e:
                        logger.warning("Home room could not be created. " + str(e))

        return None


class SchedulingStrategy(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    notifications_emails = models.TextField(null=True, blank=True)
    token_registration = models.CharField(max_length=255, default=_random_token)
    scheduling_strategy = models.CharField(
        max_length=255, choices=SCHEDULING_STRATEGIES, default=SCHEDULING_STRATEGY_LEAST_UTILIZATION
    )
    tenants = models.ManyToManyField(
        Site, help_text=_("Specification which tenant is able to see this scheduling strategy")
    )

    def __str__(self):
        return self.name

    def get_current_participant_count(self):
        return self.room_set.all().aggregate(Sum("participant_count"))["participant_count__sum"] or 0

    def get_utilization(self):
        usage = 0
        for server in self.get_servers_up():
            usage = usage + server.get_utilization()
        return ceil(usage)

    def get_utilization_max(self):
        return ceil(sum(map(lambda server: server.videostream_count_max, self.get_servers_up())))

    def get_utilization_percent(self):
        utilization_max = self.get_utilization_max()
        return 0 if utilization_max <= 0 else int(round(self.get_utilization() / utilization_max, 2) * 100)

    def get_server_for_room(self):  # noqa C901 # TODO needs to be refactored due to very high complexity
        server_to_be = None
        worker = ServerType.objects.get(name="worker")
        logger.info("Selecting new server for scheduling with strategy: {}".format(self.scheduling_strategy))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_PARTICIPANTS:
            min_count = 1000
            for server in self.server_set.filter(state=SERVER_STATE_UP, server_types__in=[worker]):
                pc = server.get_participant_count()
                if pc < min_count:
                    min_count = pc
                    server_to_be = server
                    logger.info("Best candidate so far: {}".format(server_to_be))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_UTILIZATION:
            min_count = 1000
            for server in self.server_set.filter(state=SERVER_STATE_UP, server_types__in=[worker]):
                pc = server.get_utilization()
                if pc < min_count:
                    min_count = pc
                    server_to_be = server
                    logger.info("Best candidate so far: {}".format(server_to_be))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED:

            def get_server_utilization_list():
                servers_utilization = []
                for server in self.server_set.filter(state=SERVER_STATE_UP, server_types__in=[worker]):
                    servers_utilization.append([server.get_utilization(), server])

                servers_utilization_sorted = sorted(servers_utilization, key=lambda x: x[0])

                return servers_utilization_sorted

            def get_servers_with_zero_utilization(server_utilization_list):
                zero_utilization_servers = []
                for utilization, server in server_utilization_list:
                    if utilization == 0:
                        zero_utilization_servers.append(server)

                return zero_utilization_servers

            def get_server_with_least_utilization(server_utilization_list, min_number_of_least_utilized_servers):
                utilizations_with_servers = defaultdict(list)
                for utilization, server in server_utilization_list:
                    utilizations_with_servers[utilization].append(server)

                sorted_utilizations_server_share = sorted(utilizations_with_servers.keys())

                selected_servers = []

                for utilization in sorted_utilizations_server_share:
                    selected_servers += utilizations_with_servers[utilization]

                    if len(selected_servers) >= min_number_of_least_utilized_servers:
                        break

                return selected_servers

            def randomy_pick_server_from_list(candidates):
                return random.choice(candidates)

            min_server_candidates = 5
            min_servers_candidates_with_zero_utilization = 5
            server_utilization_list = get_server_utilization_list()

            servers_with_zero_utilization = get_servers_with_zero_utilization(server_utilization_list)
            if len(servers_with_zero_utilization) >= min_servers_candidates_with_zero_utilization:
                server_to_be = randomy_pick_server_from_list(servers_with_zero_utilization)
            else:
                least_utilized_servers = get_server_with_least_utilization(
                    server_utilization_list, min_server_candidates
                )
                server_to_be = randomy_pick_server_from_list(least_utilized_servers)

            logger.info(
                "Best candidate so far = {} from server utilization list: {}".format(
                    server_to_be, server_utilization_list
                )
            )

        logger.info("Selected : {}".format(server_to_be))

        return server_to_be

    def get_servers_up(self):
        return self.server_set.filter(state=SERVER_STATE_UP)


class RoomConfiguration(models.Model):
    name = models.CharField(max_length=255, unique=True)
    mute_on_start = models.BooleanField(default=True)
    moderation_mode = models.CharField(max_length=16, choices=MODERATION_MODE, default=MODERATION_MODE_MODERATORS)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=16, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    only_prompt_guests_for_access_code = models.BooleanField(default=False)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)
    dialNumber = models.CharField(max_length=255, null=True, blank=True)
    logoutUrl = models.CharField(max_length=255, null=True, blank=True)
    welcome_message = models.CharField(max_length=255, null=True, blank=True)
    maxParticipants = models.PositiveIntegerField(blank=True, null=True)
    streamingUrl = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class ServerType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Server(models.Model):
    scheduling_strategy = models.ForeignKey(SchedulingStrategy, on_delete=models.SET_NULL, null=True)
    server_types = models.ManyToManyField(ServerType)
    dns = models.CharField(max_length=255, unique=True)
    state = models.CharField(max_length=255, choices=SERVER_STATES, default=SERVER_STATE_WAITING_RESPONSE)
    datacenter = models.CharField(max_length=255, null=True, blank=True)
    shared_secret = models.CharField(max_length=255, null=True, blank=True)
    participant_count_max = models.PositiveIntegerField(default=250, validators=[MinValueValidator(1)])
    videostream_count_max = models.PositiveIntegerField(default=25, validators=[MinValueValidator(1)])
    machine_id = models.CharField(max_length=255, unique=True, null=True, blank=True)

    last_health_check = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = (
            "scheduling_strategy",
            "dns",
        )

    def __str__(self):
        return self.dns

    # quick and dirty util cals.
    def get_utilization(self):
        audio_video_factor = self.participant_count_max / self.videostream_count_max
        audio_usage = self.get_participant_count() / audio_video_factor
        video_usage = self.get_videostream_count()
        total_usage = audio_usage + video_usage

        return 0 if total_usage <= 0 else total_usage

    def get_utilization_percent(self):
        return int(round((self.get_utilization()) / self.videostream_count_max, 2) * 100)

    def collect_stats(self):
        logger.info("Health Check started for {}".format(self.dns))
        # skip disabled servers
        if self.state != SERVER_STATE_DISABLED:
            try:
                bbb = BigBlueButton(self.dns, self.shared_secret)
                meetings = bbb.validate_get_meetings(bbb.get_meetings())
                logger.debug("Validated meetings for {} are {}".format(self.dns, meetings))

                if meetings:
                    for meeting in meetings:
                        logger.debug("Starting transaction for updating room {}".format(meeting["meetingName"]))
                        with transaction.atomic():
                            default_arguments = {
                                "server": self,
                                "meeting_id": meeting["meetingID"],
                                "state": ROOM_STATE_ACTIVE,
                                "attendee_pw": meeting["attendeePW"],
                                "moderator_pw": meeting["moderatorPW"],
                                "scheduling_strategy": self.scheduling_strategy,
                                "participant_count": meeting["participantCount"],
                                "videostream_count": meeting["videoCount"],
                                "is_breakout": True if meeting["isBreakout"] == "true" else False,
                                "last_running": now(),
                            }
                            if (
                                not Room.objects.filter(name=meeting["meetingName"]).exists()
                                and not default_arguments["is_breakout"]
                            ):
                                ExternalRoom.objects.update_or_create(
                                    name=meeting["meetingName"], defaults=default_arguments
                                )
                                logger.debug(
                                    "Update external meeting name ({}) to ".format(meeting["meetingName"])
                                    + "reflect server state."
                                )
                            else:
                                Room.objects.update_or_create(name=meeting["meetingName"], defaults=default_arguments)
                                logger.debug(
                                    "Update meeting name ({}) to ".format(meeting["meetingName"])
                                    + "reflect server state."
                                )
                        logger.debug("Ending transaction for updating room {}".format(meeting["meetingName"]))

                self.state = SERVER_STATE_UP

            except Exception as e:
                logger.error("setting server state of {} to ERROR".format(self.dns) + str(e))
                self.state = SERVER_STATE_ERROR

            self.last_health_check = now()
            self.save()
        logger.info("Health Check finished for {}".format(self.dns))

    def get_participant_count(self):
        return self.room_set.all().aggregate(Sum("participant_count"))["participant_count__sum"] or 0

    def get_videostream_count(self):
        return self.room_set.all().aggregate(Sum("videostream_count"))["videostream_count__sum"] or 0


class Room(models.Model):
    # Check on delete
    scheduling_strategy = models.ForeignKey(SchedulingStrategy, on_delete=models.PROTECT)
    server = models.ForeignKey(Server, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=255, unique=True)
    # Check on delete
    config = models.ForeignKey(
        RoomConfiguration, default=get_default_room_config_id, on_delete=models.SET(get_default_room_config)
    )
    meeting_id = models.CharField(max_length=255, unique=True, default=_random_meeting_id)
    is_public = models.BooleanField(default=False)
    state = models.CharField(max_length=30, choices=ROOM_STATE, default=ROOM_STATE_INACTIVE)

    # Room Config
    mute_on_start = models.BooleanField(default=True)
    moderation_mode = models.CharField(max_length=16, choices=MODERATION_MODE, default=MODERATION_MODE_MODERATORS)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=16, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    only_prompt_guests_for_access_code = models.BooleanField(default=False)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)
    dialNumber = models.CharField(max_length=255, null=True, blank=True)
    logoutUrl = models.CharField(max_length=255, null=True, blank=True)
    welcome_message = models.CharField(max_length=255, null=True, blank=True)
    maxParticipants = models.PositiveIntegerField(blank=True, null=True)
    streamingUrl = models.CharField(max_length=255, null=True, blank=True)

    attendee_pw = models.CharField(max_length=255, default=_random_token)
    moderator_pw = models.CharField(max_length=255, default=_random_token)

    direct_join_secret = models.CharField(max_length=255, blank=True)

    comment_public = models.CharField(max_length=255, null=True, blank=True)
    comment_private = models.CharField(max_length=255, null=True, blank=True)

    click_counter = models.PositiveIntegerField(default=0)

    participant_count = models.PositiveIntegerField(default=0)
    videostream_count = models.PositiveIntegerField(default=0)

    is_breakout = models.BooleanField(default=False)

    event_collection_strategy = models.CharField(max_length=255, null=True, blank=True)
    event_collection_parameters = models.TextField(null=True, blank=True)

    last_meeting_creating = models.DateTimeField(null=True, blank=True)
    last_running = models.DateTimeField(null=True, blank=True)

    tenants = models.ManyToManyField(Site, help_text=_("Specification which tenant is able to see this room"))

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name

    @staticmethod
    def get_participants_current(tenant):
        return (
            Room.objects.filter(tenants__in=[tenant]).aggregate(Sum("participant_count"))["participant_count__sum"] or 0
        )

    @staticmethod
    def get_total_instantiation_counter():
        return Room.objects.all().aggregate(Sum("instantiation_counter"))["instantiation_counter__sum"] or 0

    def is_meeting_running(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        return bbb.is_meeting_running(self.meeting_id)

    def get_meeting_infos(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        bbb.get_meeting_infos(self.meeting_id)

    def end_meeting(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        return bbb.end(self.meeting_id, self.moderator_pw)

    def is_room_only(self):
        is_subclass = self.is_homeroom() or self.is_personalroom() or self.is_externalroom()
        return not is_subclass

    def is_homeroom(self):
        try:
            return self.homeroom is not None
        except Room.homeroom.RelatedObjectDoesNotExist:
            return False

    def is_personalroom(self):
        try:
            return self.personalroom is not None
        except Room.personalroom.RelatedObjectDoesNotExist:
            return False

    def is_externalroom(self):
        try:
            return self.externalroom is not None
        except Room.externalroom.RelatedObjectDoesNotExist:
            return False


class ExternalRoom(Room):
    pass


class HomeRoom(Room):
    owner = models.OneToOneField(User, on_delete=models.CASCADE, null=False, related_name="homeroom_ptr")


class PersonalRoom(Room):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    co_owners = models.ManyToManyField(User, related_name="co_owners", blank=True)

    def has_co_owner(self, user: User):
        return user in self.co_owners.all()

    @staticmethod
    def co_owner_shall_not_contain_owner(**kwargs):
        action: str = kwargs["action"]

        if action == "pre_add":
            instance: PersonalRoom = kwargs["instance"]
            pk_set: set = kwargs["pk_set"]

            if instance.owner.id in pk_set:
                logger.warning("Prevented storing owner ({}) as co-owner for room {}.".format(instance.owner, instance))
                pk_set.remove(instance.owner.id)


m2m_changed.connect(PersonalRoom.co_owner_shall_not_contain_owner, sender=PersonalRoom.co_owners.through)


class RoomEvent(models.Model):
    uid = models.CharField(max_length=255, primary_key=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return "{} – {}".format(self.room, self.name)

    class Meta:
        ordering = ("room",)


class GeneralParameter(models.Model):
    tenant = models.OneToOneField(
        Site,
        on_delete=models.CASCADE,
        help_text=_("Describes which tenant this general parameter is for"),
    )
    latest_news = models.TextField(
        null=True,
        blank=True,
        verbose_name="Latest news",
        help_text=_(
            "This text will be shown in a red alert box on the start page. "
            "You can use it to inform your users about critical news."
        ),
    )
    participant_total_max = models.PositiveIntegerField(default=0)
    jitsi_enable = models.BooleanField(
        default=False,
        verbose_name=_("Enable Jitsi ad-hoc meeting"),
        help_text=_(
            "If checked a button for ad-hoc meeting creation using Jitsi is enabled."
            " If enabled, please make sure to also set a target jisti server in the jits_url field"
        ),
    )
    jitsi_url = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        default="https://meet.jit.si/",
        verbose_name=_("JITSI URL"),
        help_text=_("Defines the target of the create ad-hoc meeting button on the start page."),
    )
    playback_url = models.CharField(max_length=255, blank=True, null=True)
    download_url = models.CharField(max_length=255, blank=True, null=True)
    faq_url = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("FAQ URL"),
        help_text=_("Defines the target of the read FAQ button on the start page."),
    )
    # TODO Check if still used. Dont think so....
    feedback_email = models.CharField(max_length=255, blank=True, null=True)
    footer = models.TextField(
        blank=True, null=True, verbose_name=_("Footer"), help_text=_("Defines the footer text of the application")
    )
    logo_link = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        verbose_name=_("Logo URL"),
        help_text=_("The image behind this link will be used as logo on this application."),
    )
    favicon_link = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        verbose_name=_("FAV Icon URL"),
        help_text=_("The image behind this link will be used as fav icon on this application."),
    )
    page_title = models.CharField(
        max_length=510,
        blank=True,
        null=True,
        default="bbb@scale | Virtual Rooms",
        verbose_name=_("Page Title"),
        help_text=_("Define the title of this application."),
    )
    app_title = models.CharField(
        max_length=255,
        default="Virtual Rooms",
        verbose_name=_("App Title"),
        help_text=_("Define the application title showed beside the logo."),
    )
    home_room_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enable Home room"),
        help_text=_(
            "If checked the Home room feature is enabled and users "
            "have access to their personal room via a button on the start page."
        ),
    )
    home_room_teachers_only = models.BooleanField(
        default=True,
        verbose_name=_("Teachers only"),
        help_text=_("If checked only teachers are allowed to access their private rooms."),
    )
    home_room_scheduling_strategy = models.ForeignKey(
        SchedulingStrategy,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Scheduling Strategy"),
        help_text=_("Define which scheduling stragtegy " + "will be used for new Home rooms."),
    )
    home_room_room_configuration = models.ForeignKey(
        RoomConfiguration,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Room configuration"),
        help_text=_("Define which room configuration will be " + "used for new Home rooms."),
    )
    default_theme = models.ForeignKey(
        "Theme",
        on_delete=models.PROTECT,
        verbose_name=_("Default Theme"),
        help_text=_(
            "Define which theme should be the default theme for users "
            + "who have not changed their theme yet or for non logged in users."
        ),
    )
    personal_rooms_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Enable personal room"),
        help_text=_("If checked the personal room feature is enabled and users can create personal rooms"),
    )
    personal_rooms_scheduling_strategy = models.ForeignKey(
        SchedulingStrategy,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="personal_room_scheduling_strategy",
        verbose_name=_("Personal Room Scheduling Strategy"),
        help_text=_("Define which scheduling strategy will be used for personal rooms."),
    )
    personal_rooms_teacher_max_number = models.IntegerField(
        default=0,
        verbose_name=_("Teacher max number"),
        help_text=_(
            "Defines the maximum number of rooms a teacher can "
            "create. Can be overwritten on a per user basis using "
            "the user field."
        ),
    )
    personal_rooms_non_teacher_max_number = models.IntegerField(
        default=0,
        verbose_name=_("Non-teacher max number"),
        help_text=_(
            "Defines the maximum number of rooms a non-teacher "
            "can create. "
            "Can be overwritten on a per user basis "
            "using the user field."
        ),
    )
    enable_occupancy = models.BooleanField(
        default=True,
        verbose_name=_("Enable occupancy columns"),
        help_text=_('If checked the columns "Current/Next Occupancy" are enabled on the front page'),
    )
    enable_recordings = models.BooleanField(
        default=True,
        verbose_name=_("Enables recordings for tenant"),
        help_text=_("If checked the recordings option will show up while starting a room"),
    )

    hide_statistics_enable = models.BooleanField(
        default=False,
        verbose_name=_("Hide statistics page from anonymous users"),
        help_text=_("If checked the statistics page will be hidden from anonymous users."),
    )

    hide_statistics_logged_in_users = models.BooleanField(
        default=False,
        verbose_name=_("Hide statistics page from logged in users"),
        help_text=_("If checked the statistics page will be hidden from logged in users."),
    )

    show_personal_rooms = models.BooleanField(
        default=True,
        verbose_name=_("Show personal rooms on frontpage"),
        help_text=_("If enabled, the personal rooms tab will be shown on the frontpage"),
    )

    show_home_rooms = models.BooleanField(
        default=True,
        verbose_name=_("Show home rooms on frontpage"),
        help_text=_("If enabled, the home rooms tab will be shown on the frontpage"),
    )

    @classmethod
    def load(cls, tenant):
        return cls.objects.get_or_create(
            tenant=tenant, defaults={"default_theme": Theme.objects.order_by("pk").first()}
        )[0]

    def __str__(self) -> str:
        return self.__class__.__name__ + "(tenant=" + repr(self.tenant.name) + ")"


class Meeting(models.Model):
    room_name = models.CharField(max_length=255, null=True, blank=True)
    meeting_id = models.CharField(max_length=255, null=True, blank=True)
    creator = models.CharField(max_length=255, null=True, blank=True)
    started = models.DateTimeField(auto_now_add=True)
    replay_id = models.CharField(max_length=255, null=True, blank=True)
    replay_url = models.URLField(max_length=511, null=False, blank=True, default="")

    def __str__(self):
        return "{}-{}-{}".format(self.started.date(), self.room_name, self.creator)

    class Meta:
        ordering = ("-started",)


class Theme(models.Model):
    name = models.CharField(max_length=255, unique=True)
    base_stylesheet_static_path = models.TextField(null=True)
    multi_select_stylesheet_static_path = models.TextField(null=True)

    def __str__(self) -> str:
        return self.name

    def __getattr__(self, element: str) -> Optional[str]:
        try:
            static_path = self.__getattribute__(element + "_static_path")
        except AttributeError as exc:
            exc.args = (exc.args[0].replace("_static_path", ""), *exc.args[1:])
            raise

        if static_path is None:
            return None

        return static(static_path)


class Webhook(models.Model):
    name = models.CharField(max_length=255, unique=True, help_text=_("Free-text name field"))

    url = models.CharField(max_length=255, help_text=_("The webhook receiver's URL"))

    event = models.CharField(
        max_length=255,
        choices=WEBHOOKS,
        help_text=_("The event this Webhook is triggered on"),
    )

    enabled = models.BooleanField(
        default=True,
        help_text=_("Whether this Webhook is active"),
    )

    secret = models.CharField(
        max_length=255,
        help_text=_(
            "Hex-encoded secret for authenticating webhooks. "
            "Used to compute a HMAC-SHA512 over the request body and a timestamp. "
            "The HMAC (but not the secret!) is included with the request in "
            "the X-Hook-Signature header."
        ),
    )

    timeout = models.DurationField(
        default=timedelta(seconds=10),
        help_text=_("Set a timeout for the webhook request to complete"),
    )

    num_retries = models.IntegerField(
        default=3,
        help_text=_("Retry failed execution this many times"),
    )

    def __str__(self) -> str:
        return self.name
